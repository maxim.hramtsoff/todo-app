import React , { Component } from 'react';
import ReactDOM from 'react-dom';

import AppHeader from "./components/app-header/app-header";
import SearchPanel from "./components/search-input/search-input";
import ToDoList from "./components/todo-list/todo-list"
import ItemStatusFilter from "./components/item-staus-filter/item-status-filter";
import ItemAddForm from "./components/item-add-form/item-add-form"

import './index.css';

class App extends Component {
     maxId = 100;

     state = {
        toDoData : [
            { label : 'Drink Coffee', important: false, id : 1 },
            { label : 'Make Awesome App', important: true, id : 2 },
            { label : 'have a lunch', important: false, id: 3 }
        ]
    }

    deleteItem = (id) => {
        this.setState(({ toDoData }) => {
            const idx = toDoData.findIndex((el) => el.id === id);

            const newArray = [
                ...toDoData.slice(0, idx),
                ...toDoData.slice(idx + 1)
            ];

            return {
                toDoData: newArray
            }

        })
     }

     addItem = (text) =>  {
         const newItem = {
             label: text,
             important: false,
             id: this.maxId++
         };

         this.setState(({toDoData}) => {
             const newArr = [
                 ...toDoData,
                 newItem
             ];

             return {
                 toDoData: newArr
             }
         });
     }

    render() {
        return (
            <div className= 'todo-app'>
                <AppHeader toDo={1} done={3}/>
                <div className='top-panel d-flex'>
                    <SearchPanel />
                    <ItemStatusFilter />
                </div>

                <ToDoList todos = { this.state.toDoData }
                        onDeleted = { this.deleteItem } />
                <ItemAddForm onAdd = { this.addItem } />
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
